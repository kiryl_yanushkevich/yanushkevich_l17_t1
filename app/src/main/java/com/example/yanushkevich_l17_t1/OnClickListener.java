package com.example.yanushkevich_l17_t1;

public interface OnClickListener {
    void onClick(int position);
}
