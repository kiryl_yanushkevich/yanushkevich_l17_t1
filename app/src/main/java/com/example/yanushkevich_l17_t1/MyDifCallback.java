package com.example.yanushkevich_l17_t1;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

public class MyDifCallback extends DiffUtil.Callback {

    private final List<House> mOldList;
    private final List<House> mNewList;

    public MyDifCallback(List<House> mNewList, List<House> mOldList){
        this.mNewList = mNewList;
        this.mOldList = mOldList;
    }

    @Override
    public int getOldListSize() {
        return mOldList != null ? mOldList.size(): 0;
    }

    @Override
    public int getNewListSize() {
        return mNewList != null ? mNewList.size():0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        House oldHouse = mOldList.get(oldItemPosition);
        House newHouse = mNewList.get(newItemPosition);
        return oldHouse.getTitle().equals(newHouse.getTitle());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return true;
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        House oldHouse = mOldList.get(oldItemPosition);
        House newHouse = mNewList.get(newItemPosition);

        Bundle diff =new Bundle();
        if (!(newHouse.getHousePicture() == oldHouse.getHousePicture())) {
            diff.putInt("imageId", newHouse.getHousePicture());
        }
        if (!newHouse.getTitle().equals(oldHouse.getTitle())) {
            diff.putString("title", newHouse.getTitle());
        }
        if (!newHouse.getDescription().equals(oldHouse.getDescription())) {
            diff.putString("description", newHouse.getDescription());
        }
        if (diff.size() == 0) {
            return null;
        }
        return diff;
    }
}
