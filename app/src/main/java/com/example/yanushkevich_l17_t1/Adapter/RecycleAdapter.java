package com.example.yanushkevich_l17_t1.Adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yanushkevich_l17_t1.House;
import com.example.yanushkevich_l17_t1.MyDifCallback;
import com.example.yanushkevich_l17_t1.OnClickListener;
import com.example.yanushkevich_l17_t1.R;

import java.util.List;

public class RecycleAdapter extends RecyclerView.Adapter<AdapterHolder> {

    private OnClickListener onDeleteButtonClick =null;
    private List<House> houseList;

    public RecycleAdapter(List<House> houseList){
        this.houseList = houseList;
    }

    @NonNull
    @Override
    public AdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.house_descritrion, parent, false);
        return new AdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterHolder holder, final int position) {
        holder.houseImage.setImageResource(houseList.get(position).getHousePicture());
        holder.description.setText(houseList.get(position).getDescription());
        holder.title.setText(houseList.get(position).getTitle());
    }

    @Override
    public void onBindViewHolder(
            @NonNull final AdapterHolder holder,
            final int position,
            List<Object> payloads){
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteHouse(v, holder.getAdapterPosition());
            }
        });
        if (payloads.isEmpty()){
            super.onBindViewHolder(holder, position, payloads);
        } else{
            Bundle o = (Bundle) payloads.get(0);
            for (String key : o.keySet()){
                if (key.equals("imageId")) {
                    holder.houseImage.setImageResource(houseList.get(position).getHousePicture());
                }
                if (key.equals("title")) {
                    holder.title.setText(houseList.get(position).getTitle());
                }
                if (key.equals("description")) {
                    holder.description.setText(houseList.get(position).getDescription());
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return houseList.size();
    }

    public void setDeleteBtnListener(OnClickListener deleteBtnListener){
        this.onDeleteButtonClick = deleteBtnListener;
    }

    public void onNewDate(List<House> newHouseList){
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MyDifCallback(newHouseList, houseList));
        houseList.clear();
        houseList.addAll(newHouseList);
        diffResult.dispatchUpdatesTo(this);
    }

    private void onDeleteHouse(View view, int position){
        if (houseList.size() > 0){
            if (onDeleteButtonClick != null){
                onDeleteButtonClick.onClick(position);
            }
        } else {

            Toast.makeText(view.getContext(), "List is empty", Toast.LENGTH_SHORT).show();
        }
    }
}
