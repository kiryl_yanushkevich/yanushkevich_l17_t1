package com.example.yanushkevich_l17_t1.Adapter;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.yanushkevich_l17_t1.R;

public class AdapterHolder extends RecyclerView.ViewHolder {
    ImageButton deleteButton;
    TextView description;
    TextView title;
    ImageView houseImage;

    public AdapterHolder(View view){
        super(view);
        deleteButton = view.findViewById(R.id.delete_button);
        title = view.findViewById(R.id.cardViewTitle);
        description = view.findViewById(R.id.cardViewText);
        houseImage = view.findViewById(R.id.houseImage);
    }
}
