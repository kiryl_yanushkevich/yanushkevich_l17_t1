package com.example.yanushkevich_l17_t1;


import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class InformationContainer {

    private static List<Integer> createImageList(){
        List<Integer> imageArray = new LinkedList<>();
        imageArray.add(R.drawable.house_icon);
        imageArray.add(R.drawable.second_house_icon);
        imageArray.add(R.drawable.third_house_icon);
        imageArray.add(R.drawable.fourth_house_icon);
        imageArray.add(R.drawable.fifth_house_icon);
        imageArray.add(R.drawable.six_house_icon);
        return imageArray;
    }

    public static House createRandomHouse(){
        List<String> descriptionValues = createDescriptionList();
        List<String> titleValues = createTitle();
        List<Integer> imageValues = createImageList();
        Random random = new Random();
        int rndNumber = random.nextInt(titleValues.size());
        String title = titleValues.get(rndNumber);
        rndNumber = random.nextInt(descriptionValues.size());
        String description = descriptionValues.get(rndNumber);
        rndNumber = random.nextInt(imageValues.size());
        int imageId = imageValues.get(rndNumber);
        House house = new House(title, description, imageId);
        return house;
    }

    private static List<String> createTitle(){
        List<String> title = new LinkedList<>();
        title.add("House №1");
        title.add("House №2");
        title.add("House №3");
        title.add("House №4");
        title.add("House №5");
        title.add("House №6");
        return title;
    }

    private static List<String> createDescriptionList(){
        List<String> descriptionList = new LinkedList<>();
        descriptionList.add("Some Text №1");
        descriptionList.add("Some Text №2");
        descriptionList.add("Some Text №3");
        descriptionList.add("Some Text №4");
        descriptionList.add("Some Text №5");
        descriptionList.add("Some Text №6");
        return descriptionList;
    }

    public static List<House> createHouseGenericList(int count){
        List<House> houseList = new LinkedList<>();
        List<String> descriptionValues = createDescriptionList();
        List<String> titleValues = createTitle();
        List<Integer> imageValues = createImageList();
        Random random = new Random();
        for (int i = 0; i < count; i++){
            int rndNumber = random.nextInt(titleValues.size());
            String title = titleValues.get(rndNumber);
            rndNumber = random.nextInt(descriptionValues.size());
            String description = descriptionValues.get(rndNumber);
            rndNumber = random.nextInt(imageValues.size());
            int imageId = imageValues.get(rndNumber);
            houseList.add(new House(title, description, imageId));
        }
        return houseList;
    }
}
