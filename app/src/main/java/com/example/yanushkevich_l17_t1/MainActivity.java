package com.example.yanushkevich_l17_t1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;

import com.example.yanushkevich_l17_t1.fragment.DialogFragment;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_fragment_layout);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_example);

        if (fragment == null) {
            fragment = new DialogFragment();
            fm.beginTransaction().add(R.id.fragment_example, fragment).commit();
        }
    }
}
