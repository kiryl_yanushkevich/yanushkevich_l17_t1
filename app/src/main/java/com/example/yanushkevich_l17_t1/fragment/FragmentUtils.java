package com.example.yanushkevich_l17_t1.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class FragmentUtils {
    public static void showDialog(
            final Context context,
            String message,
            String positiveButtonText,
            String negativeButtonText,
            DialogInterface.OnClickListener onPositiveButtonClick,
            DialogInterface.OnClickListener onNegativeButtonClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton(positiveButtonText, onPositiveButtonClick)
                .setNegativeButton(negativeButtonText, onNegativeButtonClick)
                .setCancelable(false)
                .show();
    }
}
