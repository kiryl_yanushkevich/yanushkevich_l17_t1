package com.example.yanushkevich_l17_t1.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yanushkevich_l17_t1.Adapter.RecycleAdapter;
import com.example.yanushkevich_l17_t1.House;
import com.example.yanushkevich_l17_t1.InformationContainer;
import com.example.yanushkevich_l17_t1.OnClickListener;
import com.example.yanushkevich_l17_t1.R;

import java.util.ArrayList;
import java.util.List;


public class DialogFragment extends Fragment implements OnClickListener {

    private RecyclerView recyclerView;
    private RecycleAdapter recycleAdapter;
    private Button addButton;
    private List<House> houseList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);
        recyclerView = view.findViewById(R.id.house_list);
        addButton = view.findViewById(R.id.button);
        initializedRecyclerView();
        initializedAddButton();

        return view;
    }

    @Override
    public void onClick(final int position) {
        FragmentUtils.showDialog(getContext(),
                "you want delete house?",
                "yes",
                "no",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        deleteHouse(position);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        dialog.cancel();
                    }
                });
    }

    private void initializedRecyclerView(){
        houseList = InformationContainer.createHouseGenericList(3);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recycleAdapter = new RecycleAdapter(houseList);
        recycleAdapter.setDeleteBtnListener(this);
        recyclerView.setAdapter(recycleAdapter);
    }

    private void initializedAddButton() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addHouse();
            }
        });
    }

    private void deleteHouse(int position){
        List<House> newHouseList = new ArrayList<>(houseList);
        newHouseList.remove(position);
        recycleAdapter.onNewDate(newHouseList);
        houseList.clear();
        houseList.addAll(newHouseList);
        //recyclerView.smoothScrollToPosition(houseList.size() - 1);
    }

    private void addHouse(){
        List<House> newHouseList = new ArrayList<>(houseList);
        newHouseList.add(InformationContainer.createRandomHouse());
        recycleAdapter.onNewDate(newHouseList);
        houseList.clear();
        houseList.addAll(newHouseList);
        recyclerView.smoothScrollToPosition(houseList.size() - 1);
    }
}
