package com.example.yanushkevich_l17_t1;

public class House {
    private String title;
    private String description;
    private int housePicture;

    public House(String title, String description, int pictureId){
        this.title = title;
        this.description = description;
        this.housePicture = pictureId;
    }

    public House(House house){
        this.title = house.getTitle();
        this.description = house.getDescription();
        this.housePicture = house.getHousePicture();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHousePicture() {
        return housePicture;
    }

    public void setHousePicture(int housePicture) {
        this.housePicture = housePicture;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result;
        if (obj == this){
            result = true;
        } else if (obj == null || obj.getClass() != this.getClass()){
            result = false;
        } else {
            House house = (House) obj;
            result = (housePicture == house.housePicture && (title == house.title || (title != null && title.equals(house.getTitle())))
                    && (description == house.description || (description != null && description.equals(house.getDescription()))));
        }
        return result;
    }
}
